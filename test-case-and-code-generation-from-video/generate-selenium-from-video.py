import streamlit as st
import os
import vertexai
from vertexai.generative_models import (
    GenerationConfig,
    GenerativeModel,
    HarmBlockThreshold,
    HarmCategory,
    Part,
)
import re
import vertexai.generative_models as generative_models
from vertexai.preview.vision_models import (
    ImageGenerationModel,
    MultiModalEmbeddingModel,
)
from vertexai.language_models import TextEmbeddingInput, TextEmbeddingModel

PROJECT_ID = os.environ.get("GCP_PROJECT")  # Your Google Cloud Project ID
LOCATION = os.environ.get("GCP_REGION")  # Your Google Cloud Project Region
vertexai.init(project=PROJECT_ID, location=LOCATION)

model_gemini_pro = GenerativeModel("gemini-1.0-pro")
model_gemini_pro_15 = GenerativeModel("gemini-1.5-pro-preview-0514")
model_gemini_flash = GenerativeModel("gemini-1.5-flash-preview-0514")
model_experimental = GenerativeModel("gemini-experimental")
multimodal_model_pro = GenerativeModel("gemini-1.0-pro-vision")
multimodal_embeddings = MultiModalEmbeddingModel.from_pretrained(
    "multimodalembedding@001"
)
embeddings = TextEmbeddingModel(model_id="textembedding-gecko-multilingual@latest")
imagen = ImageGenerationModel.from_pretrained("imagegeneration@005")

safety_settings = {
    generative_models.HarmCategory.HARM_CATEGORY_HATE_SPEECH: generative_models.HarmBlockThreshold.BLOCK_NONE,
    generative_models.HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT: generative_models.HarmBlockThreshold.BLOCK_NONE,
    generative_models.HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT: generative_models.HarmBlockThreshold.BLOCK_NONE,
    generative_models.HarmCategory.HARM_CATEGORY_HARASSMENT: generative_models.HarmBlockThreshold.BLOCK_NONE,
}

def sendPrompt(input, model):    
    token_size = model.count_tokens(input)
    token_size = str(token_size)
    patternToken = r"total_tokens:\s*(\d+)"
    matchToken = re.search(patternToken, token_size)

    total_tokens = int(matchToken.group(1))
    if total_tokens > 1000000:
        raise ValueError("Total tokens must be less than 1000000")

    patternChar = r"total_billable_characters:\s*(\d+)"
    matchChar = re.search(patternChar, token_size)

    billable_characters = int(matchChar.group(1))
    valor = (billable_characters / 1000) * 0.0025

    prompt_response = model.generate_content(input,
        generation_config={
            "max_output_tokens": 8192,
            "temperature": 0.4,
            "top_p": 1
        },
        safety_settings=safety_settings,
    )
    return prompt_response.text


def reset_page_state(prefix: str):
    for key in st.session_state:
        if key.startswith(prefix):
            del st.session_state[key]


def reset_st_state():
    for key in st.session_state:
        del st.session_state[key]

if reset := st.button("Reset Demo State"):
    reset_st_state()

if 'response' not in st.session_state:
    st.session_state['response'] = 'init'


@st.cache_resource
def load_models(name):
    """
    Load the generative models for text and multimodal generation.

    Returns:
        Tuple: A tuple containing the text model and multimodal model.
    """
    text_model_pro = GenerativeModel(name)
    multimodal_model_pro = GenerativeModel(name)
    return text_model_pro, multimodal_model_pro


def get_gemini_pro_text_response(
    model: GenerativeModel,
    contents: str,
    generation_config: GenerationConfig,
    stream: bool = True,
):
    safety_settings = {
        HarmCategory.HARM_CATEGORY_HARASSMENT: HarmBlockThreshold.BLOCK_NONE,
        HarmCategory.HARM_CATEGORY_HATE_SPEECH: HarmBlockThreshold.BLOCK_NONE,
        HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT: HarmBlockThreshold.BLOCK_NONE,
        HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT: HarmBlockThreshold.BLOCK_NONE,
    }

    responses = model.generate_content(
        prompt,
        generation_config=generation_config,
        safety_settings=safety_settings,
        stream=stream,
    )

    final_response = []
    for response in responses:
        try:
            # st.write(response.text)
            final_response.append(response.text)
        except IndexError:
            # st.write(response)
            final_response.append("")
            continue
    return " ".join(final_response)


def get_gemini_pro_vision_response(
    model, prompt_list, generation_config={}, stream: bool = True
):
    generation_config = {"temperature": 0.1, "max_output_tokens": 2048}
    responses = model.generate_content(
        prompt_list, generation_config=generation_config, stream=stream
    )
    final_response = []
    for response in responses:
        try:
            final_response.append(response.text)
        except IndexError:
            pass
    return "".join(final_response)


st.header("Generate User Test with Selenium", divider="rainbow")

st.markdown("""
This demonstration showcases the capabilities of our Gemini Models. 
            
In this example, you'll witness how it can:

- Analyze Videos: Understand the actions taking place in a video.
- Generate Descriptions: Create detailed text descriptions of video content.
- Craft Code: Automatically generate code based on a video and description.
            
This demo focuses on creating a Selenium script, a powerful tool for automating web browser interactions.

Feel free to explore and see how this technology can assist you in your coding endeavors! :rocket:
""")

model_name = st.radio(
      label="Model:",
      options=["gemini-experimental", "gemini-1.5-pro-preview-0514", "gemini-1.5-flash-preview-0514"],
      captions=["Gemini Pro Experimental", "Gemini Pro 1.5", "Gemini Flash 1.5"],
      key="model_name",
      index=0,
      horizontal=True)

text_model_pro, multimodal_model_pro = load_models(model_name)

prompt = ""

st.divider()

st.markdown( """Gemini Pro Vision can also provide the description of what is going on in the video:""" )
vide_desc_uri = "gs://ai-selenium-videos/SQBI_Boleto.mp4"
video_desc_url = ("https://storage.googleapis.com/" + vide_desc_uri.split("gs://")[1])

if vide_desc_uri:
    vide_desc_img = Part.from_uri(vide_desc_uri, mime_type="video/mp4")
    st.video(video_desc_url)
    st.write("Our expectation: Generate the description of the video")
    prompt = """Describe what is happening in the video and answer the following questions: \n


            **Context:** I have a video showcasing a series of tasks being performed on a web browser. I need a detailed description of each action taken. 

            **Input:** [video_data]

            **Output:**

            * **Timestamped list of actions:** Provide a list of actions taken in the video, along with their timestamps (e.g., "0:10 - User clicks the 'Sign In' button.").
            * **Detailed description:** For each action, provide a clear and concise description of what is happening. Include:
                    * **What is being clicked/selected/typed?**
                    * **What is the purpose of the action?**
                    * **Any relevant context or information displayed on the screen.**
            * **Visual aids:** If possible, include annotations of the video to help visualize the actions.

            **Example:**

                **Timestamp:** 0:10
                **Action:** User clicks the "Sign In" button.
                **Description:** The user clicks the blue "Sign In" button located in the top right corner of the screen. This action initiates the login process.

            **Additional notes:**

                * **Focus on user interactions:** The description should focus on the user's actions and the visible results on the screen.
                * **Avoid technical jargon:** Use clear and simple language that anyone can understand.
                * **Pay attention to details:** Capture all relevant information, including specific button names, website URLs, and any error messages.

                **Please note:** The more detailed and accurate your description is, the more helpful it will be for me to understand the video.

            """
    tab1, tab2 = st.tabs(["Response", "Prompt"])
    vide_desc_description = st.button(
        "Generate", key="vide_desc_description"
    )
    with tab1:
        if vide_desc_description and prompt:
            with st.spinner(
                "Generating video description using Gemini Pro..."
            ):
                response = get_gemini_pro_vision_response(
                    multimodal_model_pro, [prompt, vide_desc_img]
                )
                st.markdown(response)
                st.markdown("\n\n\n")
                st.session_state["response"] = response
    with tab2:
        st.write("Prompt used:")
        st.write(prompt, "\n", "{video_data}")

st.divider()
generate_selenium = st.button("Criar selenium", key="generate_selenium")
if generate_selenium:
    with st.spinner("Generating your selenium code using Gemini..."):
        first_tab1, first_tab2= st.tabs(["Code", "Prompt"])
        with first_tab1:
            promptSelenium = """use the content in the video plus the description to create a selenium script to automate the task""" + "\n" + st.session_state["response"]
            vide_desc_img = Part.from_uri(vide_desc_uri, mime_type="video/mp4")
            if promptSelenium:
                response = get_gemini_pro_vision_response( multimodal_model_pro, [promptSelenium, vide_desc_img])
                st.markdown(response)
                st.markdown("\n\n\n")
        with first_tab2:
            st.write("Prompt used:")
            st.write(promptSelenium, "\n", "{video_data}")