import streamlit as st
import os
import re
import vertexai
from vertexai.generative_models import (
    GenerationConfig,
    GenerativeModel,
    HarmBlockThreshold,
    HarmCategory,
    Part,
)
import vertexai.generative_models as generative_models
from vertexai.preview.vision_models import (
    ImageGenerationModel,
    MultiModalEmbeddingModel,
)
from vertexai.language_models import TextEmbeddingInput, TextEmbeddingModel

def load_models(model_name):
    if model_name == "gemini-experimental":
        model = model_experimental
    elif model_name == "gemini-1.5-pro-preview-0514":
        model = model_gemini_pro_15
    else:
        model = model_gemini_flash
    return model 

def reset_page_state(prefix: str):
    for key in st.session_state:
        if key.startswith(prefix):
            del st.session_state[key]

def reset_st_state():
    for key in st.session_state:
        del st.session_state[key]

if reset := st.button("Reset Demo State"):
    reset_st_state()

if 'response' not in st.session_state:
    st.session_state['response'] = 'init'


#VertexAi Utils
PROJECT_ID = os.environ.get("GCP_PROJECT")  # Your Google Cloud Project ID
LOCATION = os.environ.get("GCP_REGION")  # Your Google Cloud Project Region
vertexai.init(project=PROJECT_ID, location=LOCATION)

model_gemini_pro = GenerativeModel("gemini-1.0-pro")
model_gemini_pro_15 = GenerativeModel("gemini-1.5-pro-preview-0514")
model_gemini_flash = GenerativeModel("gemini-1.5-flash-preview-0514")
model_experimental = GenerativeModel("gemini-experimental")
multimodal_model_pro = GenerativeModel("gemini-1.0-pro-vision")
multimodal_embeddings = MultiModalEmbeddingModel.from_pretrained(
    "multimodalembedding@001"
)
embeddings = TextEmbeddingModel(model_id="textembedding-gecko-multilingual@latest")
imagen = ImageGenerationModel.from_pretrained("imagegeneration@005")

safety_settings = {
    generative_models.HarmCategory.HARM_CATEGORY_HATE_SPEECH: generative_models.HarmBlockThreshold.BLOCK_NONE,
    generative_models.HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT: generative_models.HarmBlockThreshold.BLOCK_NONE,
    generative_models.HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT: generative_models.HarmBlockThreshold.BLOCK_NONE,
    generative_models.HarmCategory.HARM_CATEGORY_HARASSMENT: generative_models.HarmBlockThreshold.BLOCK_NONE,
}

def sendPrompt(input, model, contextFile):
    token_size = model.count_tokens(input)
    token_size = str(token_size)
    patternToken = r"total_tokens:\s*(\d+)"
    matchToken = re.search(patternToken, token_size)

    total_tokens = int(matchToken.group(1))
    if total_tokens > 1000000:
        raise ValueError("Total tokens must be less than 1000000")

    patternChar = r"total_billable_characters:\s*(\d+)"
    matchChar = re.search(patternChar, token_size)

    billable_characters = int(matchChar.group(1))
    valor = (billable_characters / 1000) * 0.0025

    prompt_response = model.generate_content(
        [contextFile, input],
        generation_config={
            "max_output_tokens": 8192,
            "temperature": 0.4,
            "top_p": 1
        },
        safety_settings=safety_settings,
    )
    return prompt_response.text


# Demo
st.subheader("Generate Test Case from a User Story Document")
# Story premise
model_name = st.radio(
      label="Model:",
      options=["gemini-1.5-flash-preview-0514", "gemini-experimental", "gemini-1.5-pro-preview-0514"],
      captions=["Gemini Flash 1.5", "Gemini Pro Experimental", "Gemini Pro 1.5"],
      key="model_name",
      index=0,
      horizontal=True)

model = load_models(model_name)

uploaded_file = st.file_uploader("Choose a file", type=["pdf"])
if uploaded_file is not None:
    # To read file as bytes:
    bytes_data = uploaded_file.getvalue()
    #st.write(bytes_data)
    
    uploadedDocument = Part.from_data(
        mime_type="application/pdf",
        data=bytes_data)


prompt = f"""Given a document, your task is to extract the User Story from it and Write a test case based on that User Story defined on the document. \n
      Consider the following structure format for each test case on your response:
        ## Step
        [Step number]
        ### Test Data
        1. Executar etapas Iniciais
        2. [Step of the test case].
        3. [Step of the test case].
        4. [Step of the test case].
        5. [Step of the test case].
        6. Executar etapas finais

        ### Expected Result
        1. [Expected result of the test case].
        2. [Expected result of the test case].
        3. [Expected result of the test case].
        4. [Expected result of the test case].
        5. [Expected result of the test case].
        6. [Expected result of the test case]. 
        7. [Expected result of the test case]. 
        8. [Expected result of the test case]. 
        9. [Expected result of the test case]. 
        10. [Expected result of the test case]. 
"""

generate_t2t = st.button("Generate my test case", key="generate_t2t")
if generate_t2t and prompt and uploadedDocument:
    with st.spinner("Generating your story using Gemini ..."):
        first_tab1, first_tab2= st.tabs(["Story", "Prompt"])
        with first_tab1:
            responseTestCase = sendPrompt(prompt, model, uploadedDocument)
            if responseTestCase:
                st.write("Your test case:")
                st.markdown(responseTestCase)
                st.session_state["response"] = responseTestCase
        with first_tab2:
            st.text(prompt)
        
