# Getting Started Guide 

1. Environment Setup:
Install dependencies: Navigate to the project directory and run pip install -r requirements.txt to install the required Python libraries.

Set up environment variables:
```bash 
export GCP_PROJECT="projectid" # Set this to your Google Cloud Platform project ID.
export GCP_REGION="us-central1" # Set this to the region where you want to run the application.
```

2. Run demo:
```bash 
streamlit run [demo-file].py 
```

3. Deploy on GCP Cloud Run
```bash 
gcloud artifacts repositories create [demo-repo-name] --repository-format=docker --location=us-central1 --description="Gemini Dev Demos"
gcloud builds submit . --config=./cloudbuild.yaml --substitutions SHORT_SHA=1.0 
```